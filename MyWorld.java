import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);
        Prepare();
    }
    public void Prepare()
    {
        Brick_Right brick_right = new Brick_Right();
        addObject(brick_right,getWidth()-80,getHeight()/2);
        
        Brick_Left brick_left = new Brick_Left();
        addObject(brick_left,80,getHeight()/2);
        
        Ball ball = new Ball();
        addObject(ball,getWidth()/2,getHeight()/2);
        
        
    }
}
